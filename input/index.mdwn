# Debian System Administrators

The Debian System Administration (DSA) team is responsible for
Debian's infrastructure.  This wiki is focussed on system administration
documentation, primarily in the form of HOWTOs, for users and administrators.

To contact us, mail debian-admin@lists.debian.org.

For a list of servers check
[ud-ldap](https://db.debian.org/machines.cgi), or
[munin](https://munin.debian.org/), or
[nagios](https://nagios.debian.org/)
(try dsa-guest, dsa-guest).

## source repositories

Copies of our VCS repositories are available on
[salsa](https://salsa.debian.org/dsa-team). If you want to
prepare patches for any of our GIT repositories please use git format-patch and
mail them to debian-admin@lists.debian.org or attach them to the corresponding RT
ticket.

## documentation for end users or services owners

* [how to use RT](https://wiki.debian.org/Teams/DSA/RTUsage)
* [how to setup SSH](user/ssh)
* [how to set sudo passwords](user/sudo)
* [how to request guest access to porter machines](doc/guest-account)
* [how to use schroot on porter machines](doc/schroot)
* [how to manage service subdomains](doc/subdomains)
* [firewalling](doc/firewall)
* [sending mails](user/mail-submit)

## documentation for contributors

* [how to contribute to this wiki](doc/wiki)
* [list of DSA usertagged bugreports](https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-admin@lists.debian.org)

## documentation for team members

* [tips for new and existing members](howto/tips)
* [how to update dns resource records](howto/dns)
* [how to add a new machine](howto/new-machine)
* [[howto/decommission]]: how to decommission a machine
* [[howto/puppet-setup]]
* [[howto/upgrade-to-bookworm]]
* [[howto/upgrade-to-bullseye]]
* [[howto/install-ganeti]]: ganeti related documentation
* [[howto/ilo-https]]
* [[howto/backup]]
* [[howto/bacula-hints]]
* [[howto/exim-ca]]
* [[howto/install-kvm]]: how to setup a new kvm domain without going through d-i etc.
* [[howto/postgres]]: Random postgres stuff
* [[howto/postgres-backup]]: Less random postgres stuff
* [[howto/add-guest]]: how to add a guest to ud-ldap
* [[howto/add-account]]: how to add an account to ud-ldap (also: how to upgrade a guest)
* [[howto/lock-account]]: how to lock an account in ud-ldap
* [[howto/new-dpl]]: What to do when we have a new DPL
* [[howto/new-dsa]]: What to do when we have a new DSA member
* [[howto/static-mirroring]]: Static mirroring fu
* [[howto/lvm]]: LVM related tips
* [[howto/move-a-vm]]: how to move a VM to another ganeti cluster
* [[howto/manage-metapackages]]: how to update our metapackages
* [[howto/time]]: system time management

## ports

* [[ports/hardware-requirements]]: Hardware requirements for porter boxes

## misc

* [[hardware-wishlist]]
* [[OID-Assignments|iana]]
* [[poneys]]

## archive

* [[howto/iscsi]]: how to setup a new iscsi initiator
* [[howto/export-iscsi]]: how to export new iscsi LUNs
* [[howto/swarm-kernel]]: how to build kernels for our swarm boxes
* [[howto/drac-reset]]: how to beat the radacm rootk^Wbinary only software.
* [[howto/upgrade-to-lenny]]
* [[howto/upgrade-to-squeeze]]
* [[howto/upgrade-to-wheezy]]
* [[howto/upgrade-to-jessie]]
* [[howto/upgrade-to-stretch]]
* [[howto/upgrade-to-buster]]
* [[ports/kfreebsd]]: Open issues with the kFreeBSD ports

## Pontifications

* [2013-07-12 DSA Meeting Minutes](https://lists.debian.org/debian-project/2013/07/msg00007.html)
* [2013-06-13 DSA Team Sprint Report](https://lists.debian.org/20130621180752.GA12975@valiant.palfrader.org)

## DSA blog

* [[dsablog]] - Team blog of the DSA team 
