# Puppet infrastructure

handel.debian.org is our current puppetmaster.

## To set up a new host to be a puppet client:

Make sure you have set up the IP address for the new machine in ud-ldap.
After that run puppet on puppetmaster once, so the ferm config gets
adjusted.

        : __handel__ && puppet agent --no-daemonize --onetime --environment=production

        : ::client:: && me=$(hostname -f) && [ "$me" != "${me%debian.org}" ] && apt-get update &&
                apt-get install -y --no-install-recommends puppet ruby-augeas augeas-lenses lsb-release &&
                service puppet stop &&
                (puppet agent -t || true ) &&
                cd /var/lib/puppet/ssl/certificate_requests &&
                echo sha256sum output: && echo &&
                sha256sum $me.pem &&
                echo && echo && cd /

This will not overwrite anything yet, since handel has not signed the
client cert.  Now is the time to abort if you are getting cold feet.

## To renew the certificate for an existing host:

**If the host is the puppetmaster, see the separate section below**

	: ::client:: &&
                puppet agent --disable &&
                mv /var/lib/puppet/ssl{,.bak}

	: __handel__ &&
                puppet node clean $HOST

	: ::client:: &&
                me=$(hostname -f) && [ "$me" != "${me%debian.org}" ] &&
                (puppet agent -t || true) &&
                cd /var/lib/puppet/ssl/certificate_requests &&
                echo sha256sum output: && echo &&
                sha256sum $me.pem &&
                echo && echo && cd /

## In both cases:

Compare incoming csr request:
on handel, paste the sha256output::

        : __handel__ &&
                ud-replicate && sudo -u puppet make -C /srv/puppet.debian.org/ca/ install &&
                echo "paste sha256sum output now:" &&
                read sha256 filename &&
                cd /var/lib/puppet/ssl/ca/requests &&
                ( [ -e $filename ] || (echo "$filename does not exist."; exit 1) ) &&
                echo -e "$sha256  $filename" | sha256sum -c &&
                puppetserver ca sign --certname $(basename "$filename" .pem) &&
                echo && echo && echo &&
                echo 'cat > /var/lib/puppet/ssl/certs/ca.pem << EOF ' &&
                cat /var/lib/puppet/ssl/certs/ca.pem &&
                echo 'EOF' &&
                echo "cat > /var/lib/puppet/ssl/certs/$filename << EOF " &&
                cat /var/lib/puppet/ssl/ca/signed/$filename &&
                echo 'EOF' &&
                cd / &&
                echo 'puppet agent --enable' &&
                echo 'puppet agent -t --pluginsync' &&
                echo 'puppet agent -t --pluginsync'

and execute this on the client.

        : ::client:: copy paste the thing you just created on handel

If this is a busy mail host, you might want to stop exim before proceeding
although the config files should remain identical before and after.

Try this once if you're nervous:

        : ::client:: && puppet agent -t --pluginsync --noop

It will tell you what would have changed without actually doing it.

Then run (this will change the configs in /etc):

        : ::client:: && puppet agent -t --pluginsync

This run will start puppet after reconfiguring it, so if you are
unhappy with what just happened, you'll need to stop it again to do
repair.

Double check apt - the puppet setup usually results in duplicate apt
sources, since we ship a few under sources.list.d.  Remove any unnecessary
entries from sources.list.

On handel, make sure the certs exist for the new host


We ship a samhain config file that includes /lib and /usr/lib.  This will
almost certainly be different than the config file on the machine, so it
will result in 1000s of files changed.
You may need to run samhain update after getting puppet going.

The puppet repository is public, but we sometimes need to keep passwords
in puppet.  There are many ways to do this - hiera-gpg, ENC, etc.  We've
settled on a fairly simple one.  Log into handel, create a new manifest
in the relevant module (call it something like "params.pp").  You can add
passwords to this file.  To stop git complaining on push, make sure you
update .gitignore for the new file.  Now you can import this file where
you need passwords and use them.

## Renewing the certificate for the puppetmaster

The puppetmaster's certificate needs to be generated in the correct way so
that it is also usable for connections to puppet.d.o rather than the machine's
hostname.  As the same certificate is used both for local puppet runs and by
the puppetmaster service, the services must be restarted after updating the
certificate, or clients will fail to connect due to the service continuing to
serve the old (now revoked) certificate.  The simplest solution is to reboot
the puppetmaster server after regenerating its certificate.

        : __handel__ &&
                puppetserver ca clean --certname handel.debian.org &&
                puppetserver ca generate --ca-client --certname handel.debian.org --subject-alt-names puppet,puppet.debian.org &&
                reboot
