== Random LVM stuff ==

=== LV caching ===

We use LV caching at least on godard.d.o and smit.d.o. Here are the setup instructions:

* Make sure that both the HDD and SSD are in the same volume group.

* Make sure module loading is enabled

* Run the following command to enable LV caching for the srv LV in the vg0 VG, assuming the SSD is /dev/sda3:

{{{
  lvcreate --type cache --cachemode writethrough -L 120G -n srv_cachepool vg0/srv /dev/sda3
}}}

* It is currently not possible to resize an LV with a LVM cache. The workaround is to disable LV caching, resize the LV and reenable it.
To determine how to re-instate the cache "as was" after resizing, run the following command *before* disabling caching:

{{{
  lvs -o+cache_mode,devices -a
}}}

This will produce output similar to:

{{{
  LV                          VG  Attr       LSize  Pool                  Origin      Data%  Meta%  Move Log Cpy%Sync Convert CacheMode    Devices
  [lvol0_pmspare]             vg0 ewi------- 32.00m                                                                                        /dev/vdc(2560)
  srv                         vg0 Cwi-aoC--- <2.54t [srv_cachepool_cpool] [srv_corig] 100.00 24.21           0.00             writethrough srv_corig(0)
  [srv_cachepool_cpool]       vg0 Cwi---C--- 80.00g                                   100.00 24.21           0.00             writethrough srv_cachepool_cpool_cdata(0)
  [srv_cachepool_cpool_cdata] vg0 Cwi-ao---- 80.00g                                                                                        /dev/vdc(2576)
  [srv_cachepool_cpool_cmeta] vg0 ewi-ao---- 32.00m                                                                                        /dev/vdc(2568)
  [srv_corig]                 vg0 owi-aoC--- <2.54t                                                                                        /dev/vdb(2560)
  swap                        vg0 -wi-ao---- 10.00g                                                                                        /dev/vdc(0)
}}}

The values required to rebuild the cache "as was" are:
  * the CacheMode of the _cpool LV [writethrough]
  * the LSize of the _cpool LV [80G]
  * the root of the cachepool LV names [srv_cachepool]
  * the VG and LV names for the cached volume[vg0/srv]
  * the underlying device for the _cpool_cdata LV [/dev/vdc]

* LV caching can be disabled with the following command:

{{{
  lvconvert --uncache vg0/srv
}}}
