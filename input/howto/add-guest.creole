== Add a guest account to ud-ldap ==

Check that the new user is a Debian contributor and the request is reasonable.

Save the signed request to a file and the signed DMUP agreement to another file.

Setup the script that automates the addition of guest accounts:

{{{
	git clone git@ubergit.debian.org:dsa/guest-keyring
	ln -s ~/path/to/dsa-misc/scripts/add-guest ~/bin/dsa-add-guest
}}}

Make sure you have jetring installed.

For a guest account based on DM or NM status:

{{{
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup dm <fingerprint>
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup nm <fingerprint>
}}}

For a guest account based on a sponsor:

{{{        
	dsa-add-guest ~/dsa/debian-keyrings ~/dsa/guest-keyring ~/path/to/mail ~/path/to/dmup sponsor <fingerprint>
}}}

At the git commit prompt enter something like this:

{{{
	Add Jane Doe (RT#1234)
}}}

At the final account entry prompt:

* enter fingerprint, account name, [fml] name and forwarding address.
* enter expiry date and hosts to allow access to, per the request.
  Two months is typical.

Then close the RT ticket.

== Extend the guest account access scope ==

Should the guest ask for access to other hosts, as long as their key is
still valid and their account is not expired (or not for long),
SSH to db.debian.org, and {{{ud-guest-extend $uid -x DAYS}}}.

An LDIF will be printed. Edit it to add the new required hosts
and apply it with the suggested command.
